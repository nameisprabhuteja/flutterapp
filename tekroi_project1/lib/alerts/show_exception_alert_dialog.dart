// ignore_for_file: unrelated_type_equality_checks

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:tekroi_project1/alerts/show_alert.dart';

Future<void>? showExceptionAlert(
  BuildContext context, {
  required String title,
  required Exception exception,
}) =>
    showAlert(
      context,
      title: title,
      content: _message(exception),
      defaultActionText: 'OK',
    );

String _message(Exception exception) {
  if (exception is FirebaseException) {
    return exception.message.toString();
  }
  return exception.toString();
}
