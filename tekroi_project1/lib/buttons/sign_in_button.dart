// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:tekroi_project1/buttons/custom_elevated_button.dart';

class SignInButton extends CustomElevatedButton {
  SignInButton({
    required VoidCallback onPressed,
    required String buttonName,
    double borderRadious = 8.0,
    Color textColor = Colors.black38,
    Color buttonColor = Colors.white,
    double textSize = 16.0,
  }) : super(
          buttonName: buttonName,
          child: Text(
            buttonName,
            style: TextStyle(
              fontSize: textSize,
              color: textColor,
            ),
          ),
          onPressed: onPressed,
          borderRadius: borderRadious,
          buttonColor: buttonColor,
          textColor: textColor,
          textSize: textSize,
        );
}
