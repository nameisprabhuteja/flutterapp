import 'package:flutter/material.dart';

class CustomElevatedButton extends StatelessWidget {
  const CustomElevatedButton({
    Key? key,
    required this.onPressed,
    required this.buttonName,
    this.buttonColor = Colors.white,
    this.textColor = Colors.black38,
    this.borderRadius = 8.0,
    this.textSize = 18,
    required this.child,
  }) : super(key: key);
  final VoidCallback onPressed;
  final String buttonName;
  final Color buttonColor;
  final Color textColor;
  final double borderRadius;
  final Widget child;
  final double textSize;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 55,
      child: ElevatedButton(
        child: child,
        onPressed: onPressed,
        style: ElevatedButton.styleFrom(
          primary: buttonColor,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(borderRadius)),
        ),
      ),
    );
  }
}
