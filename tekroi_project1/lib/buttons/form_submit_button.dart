// ignore_for_file: use_key_in_widget_constructors

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tekroi_project1/buttons/custom_elevated_button.dart';

class FormSubmitButton extends CustomElevatedButton {
  FormSubmitButton({
    required String buttonName,
    required VoidCallback onPressed,
    double textSize = 20,
    Color textColor = Colors.white,
  }) : super(
          onPressed: onPressed,
          child: Text(
            buttonName,
            style: TextStyle(
              color: textColor,
              fontSize: textSize,
            ),
          ),
          borderRadius: 4.0,
          buttonColor: Colors.indigo,
          buttonName: buttonName,
        );
}
