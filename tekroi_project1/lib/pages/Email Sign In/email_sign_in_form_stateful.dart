// // ignore_for_file: prefer_const_constructors, non_constant_identifier_names, must_be_immutable, avoid_print, prefer_const_constructors_in_immutables, avoid_returning_null_for_void, deprecated_member_use, use_key_in_widget_constructors

// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';
// import 'package:tekroi_project1/alerts/show_exception_alert_dialog.dart';
// import 'package:tekroi_project1/buttons/form_submit_button.dart';
// import 'package:tekroi_project1/pages/email_sign_in_model.dart';
// import 'package:tekroi_project1/services/auth.dart';
// import 'package:tekroi_project1/services/validator.dart';

// class EmailSignInFormStateful extends StatefulWidget
//     with EmailAndPasswordValidators {
//   @override
//   State<EmailSignInFormStateful> createState() => _EmailSignInFormState();
// }

// class _EmailSignInFormState extends State<EmailSignInFormStateful> {
//   final TextEditingController _emailController = TextEditingController();
//   final TextEditingController _passwordController = TextEditingController();

//   final FocusNode _emailFocusNode = FocusNode();
//   final FocusNode _passwordFocusNode = FocusNode();

//   get _email => _emailController.text;
//   get _password => _passwordController.text;

//   bool _submitted = false;
//   bool _isLoading = false;

//   EmailSignInFormType _formType = EmailSignInFormType.signIn;

//   @override
//   void dispose() {
//     super.dispose();
//     _emailController.dispose();
//     _passwordController.dispose();
//     _emailFocusNode.dispose();
//     _passwordFocusNode.dispose();
//   }

//   void _submit() async {
//     setState(() {
//       _submitted = true;
//       _isLoading = true;
//     });
//     try {
//       final auth = Provider.of<AuthBase>(context, listen: false);

//       // CircularProgressIndicator();
//       // await Future.delayed(Duration(seconds: 5));
//       if (_formType == EmailSignInFormType.signIn) {
//         await auth.signInWithEmailPassword(_email, _password);
//       } else {
//         await auth.createWithEmailPassword(_email, _password);
//       }
//       Navigator.of(context).pop();
//     } on FirebaseAuthException catch (e) {
//       showExceptionAlert(
//         context,
//         title: 'Sign in Failed',
//         exception: e,
//       );
//     } finally {
//       setState(() {
//         _isLoading = false;
//       });
//     }
//   }

//   void _emailEditingComplete() {
//     final newFocus = widget.emailValidator.isValid(_email)
//         ? _passwordFocusNode
//         : _emailFocusNode;
//     FocusScope.of(context).requestFocus(newFocus);
//   }

//   void _toggleFormTypes() {
//     setState(() {
//       _submitted = false;
//       _formType = _formType == EmailSignInFormType.register
//           ? EmailSignInFormType.signIn
//           : EmailSignInFormType.register;
//     });
//     //_emailController.clear();
//     _passwordController.clear();
//   }

//   List<Widget> _buildContent() {
//     final primaryText =
//         _formType == EmailSignInFormType.signIn ? 'Sign in' : 'Create account';
//     final secondaryText = _formType == EmailSignInFormType.signIn
//         ? 'Need an account? Register'
//         : 'Have an account? Login';

//     bool submitEnabled = widget.emailValidator.isValid(_email) &&
//         widget.passwordValidator.isValid(_password) &&
//         !_isLoading;
//     print(submitEnabled);
//     return [
//       _buildEmailTextField(),
//       SizedBox(height: 8),
//       _buildPasswordTextField(),
//       SizedBox(height: 18),
//       FormSubmitButton(
//         onPressed: _submit,
//         buttonName: primaryText,
//       ),
//       SizedBox(height: 8),
//       TextButton(
//         style: TextButton.styleFrom(primary: Colors.black38),
//         onPressed: _toggleFormTypes,
//         child: Text(
//           secondaryText,
//         ),
//       ),
//     ];
//   }

//   TextField _buildPasswordTextField() {
//     bool ShowErrorText =
//         _submitted && !widget.passwordValidator.isValid(_password);
//     return TextField(
//       onChanged: (password) => _updateButton(),
//       focusNode: _passwordFocusNode,
//       onEditingComplete: _submit,
//       autocorrect: false,
//       textInputAction: TextInputAction.done,
//       controller: _passwordController,
//       obscureText: true,
//       decoration: InputDecoration(
//         enabled: _isLoading == false,
//         errorText: ShowErrorText ? widget.invalidPasswordText : null,
//         labelText: 'Password',
//       ),
//     );
//   }

//   TextField _buildEmailTextField() {
//     bool ShowErrorText = _submitted && !widget.emailValidator.isValid(_email);
//     return TextField(
//       onChanged: (email) => _updateButton(),
//       focusNode: _emailFocusNode,
//       autocorrect: false,
//       keyboardType: TextInputType.emailAddress,
//       textInputAction: TextInputAction.next,
//       controller: _emailController,
//       onEditingComplete: _emailEditingComplete,
//       decoration: InputDecoration(
//         enabled: _isLoading == false,
//         errorText: ShowErrorText ? widget.invalidEmailText : null,
//         labelText: 'Email',
//         hintText: 'test@test.com',
//       ),
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.all(16.0),
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.stretch,
//         mainAxisSize: MainAxisSize.min,
//         children: _buildContent(),
//       ),
//     );
//   }

//   void _updateButton() {
//     setState(() {});
//   }
// }
