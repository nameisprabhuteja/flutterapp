// ignore_for_file: prefer_const_constructors, non_constant_identifier_names, must_be_immutable, avoid_print, prefer_const_constructors_in_immutables, avoid_returning_null_for_void, deprecated_member_use, use_key_in_widget_constructors

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tekroi_project1/alerts/show_exception_alert_dialog.dart';
import 'package:tekroi_project1/buttons/form_submit_button.dart';
import 'package:tekroi_project1/pages/Email%20Sign%20In/email_sign_in_bloc.dart';
import 'package:tekroi_project1/pages/Email%20Sign%20In/email_sign_in_model.dart';
import 'package:tekroi_project1/services/auth.dart';

class EmailSignInFormBlocBased extends StatefulWidget {
  EmailSignInFormBlocBased({required this.bloc});
  final EmailSignInBloc bloc;
  static Widget create(BuildContext context) {
    final auth = Provider.of<AuthBase>(context, listen: false);
    return Provider<EmailSignInBloc>(
      create: (_) => EmailSignInBloc(auth: auth),
      child: Consumer<EmailSignInBloc>(
        builder: (_, bloc, __) => EmailSignInFormBlocBased(bloc: bloc),
      ),
      dispose: (_, bloc) => bloc.dispose(),
    );
  }

  @override
  State<EmailSignInFormBlocBased> createState() => _EmailSignInFormState();
}

class _EmailSignInFormState extends State<EmailSignInFormBlocBased> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  final FocusNode _emailFocusNode = FocusNode();
  final FocusNode _passwordFocusNode = FocusNode();

  @override
  void dispose() {
    super.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    _emailFocusNode.dispose();
    _passwordFocusNode.dispose();
  }

  void _submit() async {
    try {
      await widget.bloc.submit();
      Navigator.of(context).pop();
    } on FirebaseAuthException catch (e) {
      showExceptionAlert(
        context,
        title: 'Sign in Failed',
        exception: e,
      );
    }
  }

  void _emailEditingComplete(EmailSignInModel model) {
    final newFocus = model.emailValidator.isValid(model.email)
        ? _passwordFocusNode
        : _emailFocusNode;
    FocusScope.of(context).requestFocus(newFocus);
  }

  void _toggleFormTypes() {
    widget.bloc.toggleFormTypes();
    _emailController.clear();
    _passwordController.clear();
  }

  List<Widget> _buildContent(EmailSignInModel model) {
    model.canSubmit;
    return [
      _buildEmailTextField(model),
      SizedBox(height: 8),
      _buildPasswordTextField(model),
      SizedBox(height: 18),
      FormSubmitButton(
        onPressed: _submit,
        buttonName: model.primaryButtonText,
      ),
      SizedBox(height: 8),
      TextButton(
        style: TextButton.styleFrom(primary: Colors.black38),
        onPressed: _toggleFormTypes,
        child: Text(
          model.secondaryButtonText,
        ),
      ),
    ];
  }

  TextField _buildPasswordTextField(EmailSignInModel model) {
    return TextField(
      onChanged: widget.bloc.updatePassword,
      focusNode: _passwordFocusNode,
      onEditingComplete: _submit,
      autocorrect: false,
      textInputAction: TextInputAction.done,
      controller: _passwordController,
      obscureText: true,
      decoration: InputDecoration(
        enabled: model.isLoading == false,
        errorText: model.passwordErrorText,
        labelText: 'Password',
      ),
    );
  }

  TextField _buildEmailTextField(EmailSignInModel model) {
    return TextField(
      onChanged: widget.bloc.updateEmail,
      focusNode: _emailFocusNode,
      autocorrect: false,
      keyboardType: TextInputType.emailAddress,
      textInputAction: TextInputAction.next,
      controller: _emailController,
      onEditingComplete: () => _emailEditingComplete(model),
      decoration: InputDecoration(
        enabled: model.isLoading == false,
        errorText: model.emailErrorText,
        labelText: 'Email',
        hintText: 'test@test.com',
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<EmailSignInModel>(
        stream: widget.bloc.modelStream,
        initialData: EmailSignInModel(),
        builder: (context, snapshot) {
          final EmailSignInModel? model = snapshot.data;
          return Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: _buildContent(model!),
            ),
          );
        });
  }
}
