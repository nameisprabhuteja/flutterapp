// ignore_for_file: prefer_const_constructors, use_key_in_widget_constructors

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tekroi_project1/pages/home_page.dart';
import 'package:tekroi_project1/pages/sign_in_bloc.dart';
import 'package:tekroi_project1/pages/sign_in_page.dart';
import 'package:tekroi_project1/services/auth.dart';

class LandingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final auth = Provider.of<AuthBase>(context, listen: false);
    return StreamBuilder<User?>(
      stream: auth.authStateChanges(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.active) {
          final User? user = snapshot.data;
          if (user == null) {
            return Provider<SignInBloc>(
              create: (_) => SignInBloc(auth: auth),
              dispose: (_, bloc) => bloc.dispose(),
              child: Consumer<SignInBloc>(
                builder: (_, bloc, __) => SignInPage(bloc: bloc),
              ),
            );
          } else {
            return HomePage();
          }
        }
        return Scaffold(
          body: CircularProgressIndicator(),
        );
      },
    );
  }
}
