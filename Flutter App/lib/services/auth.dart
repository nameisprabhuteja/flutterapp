// ignore_for_file: override_on_non_overriding_member

import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

abstract class AuthBase {
  Future<User?> googleSignIn();
  Stream<User?> authStateChanges();
  Future<void> signOut();
  Future<User?> signInAnonymously();
  Future<User?> signInWithEmailPassword(String email, String password);
  Future<User?> createWithEmailPassword(String email, String password);
}

class Auth implements AuthBase {
  final _fireBaseAuth = FirebaseAuth.instance;

  @override
  Stream<User?> authStateChanges() => _fireBaseAuth.authStateChanges();
  @override
  Future<User?> signOut() async {
    await _fireBaseAuth.signOut();
    final googleSignIn = GoogleSignIn();
    await googleSignIn.signOut();
  }

  //User? get currentUser => _fireBaseAuth.currentUser;

  @override
  Future<User?> signInAnonymously() async {
    final userCredentials = await _fireBaseAuth.signInAnonymously();
    return userCredentials.user;
  }

  @override
  Future<User?> signInWithEmailPassword(String email, String password) async {
    final userCredentials = await _fireBaseAuth.signInWithEmailAndPassword(
        email: email, password: password);
    return userCredentials.user;
  }

  @override
  Future<User?> createWithEmailPassword(String email, String password) async {
    final userCredentials = await _fireBaseAuth.createUserWithEmailAndPassword(
        email: email, password: password);
    return userCredentials.user;
  }

  @override
  Future<User?> googleSignIn() async {
    final googleSignIn = GoogleSignIn();
    final googleUser = await googleSignIn.signIn();
    if (googleUser != null) {
      final googleAuth = await googleUser.authentication;
      if (googleAuth.idToken != null) {
        final userCredential = await _fireBaseAuth.signInWithCredential(
          GoogleAuthProvider.credential(
            accessToken: googleAuth.accessToken,
            idToken: googleAuth.idToken,
          ),
        );
        return userCredential.user;
      } else {
        throw FirebaseAuthException(
          code: 'ERROR_WHILE_LOGGING_IN',
          message: 'SIGN IN ERROR',
        );
      }
    } else {
      throw FirebaseAuthException(
        code: 'ERROR_USER_CANCELLED',
        message: 'USER CANCELLED SIGN IN',
      );
    }
  }
}
