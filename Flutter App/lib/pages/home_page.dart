// ignore_for_file: prefer_const_constructors, use_key_in_widget_constructors, prefer_const_literals_to_create_immutables
//import 'package:firebase/firebase.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';
import 'package:tekroi_project1/alerts/show_alert.dart';
import 'package:tekroi_project1/buttons/social_sign_in_button.dart';
import 'package:tekroi_project1/services/auth.dart';

class HomePage extends StatelessWidget {
  Future<User?> _signOut(BuildContext context) async {
    final auth = Provider.of<AuthBase>(context, listen: false);
    await auth.signOut();
    Navigator.of(context).pop();
  }

  Future<void> _logoutAlert(BuildContext context) async {
    final didSignout = await showAlert(context,
        title: 'Sign Out',
        content: 'Are you sure you want to sign out',
        defaultActionText: 'Logout',
        cancelActionText: 'Cancel');
    if (didSignout == true) {
      _signOut(context);
    }
  }

  void _searchNavigator(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        fullscreenDialog: true,
        builder: (context) => _search(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: SizedBox(
        height: 53,
        width: 53,
        child: FloatingActionButton(
          onPressed: () => _searchNavigator(context),
          child: SizedBox(
              height: 23, child: Image.asset('images/search-12-32.png')),
          elevation: 10,
        ),
      ),
      drawer: Drawer(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                ElevatedButton(
                  onPressed: () => _logoutAlert(context),
                  child: Text(
                    'Logout',
                    style: TextStyle(fontSize: 20, color: Colors.black38),
                  ),
                  style: ElevatedButton.styleFrom(
                    shape: StadiumBorder(side: BorderSide(color: Colors.black)),
                    primary: Colors.white,
                    //elevation: 0,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(120),
        child: AppBar(
          elevation: 2,
          title: Text('ARCQUID'),
          actions: [
            Padding(
              padding: const EdgeInsets.all(11),
              child: IconButton(
                onPressed: () {},
                icon: Image.asset('images/notification.png'),
              ),
            )
          ],
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: homeBody(context),
      ),
    );
  }

  Widget homeBody(BuildContext context) {
    var screensize = MediaQuery.of(context).size;
    print(screensize);
    return Stack(
      children: [
        Positioned(
          //width: screensize.width,
          child: Column(
            //mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                height: 300,
                color: Colors.grey[200],
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    SizedBox(
                      height: 125,
                      width: 125,
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 8.0),
                        child: ElevatedButton(
                          onPressed: () {},
                          child: Text('DEMO'),
                          style: ElevatedButton.styleFrom(
                            fixedSize: Size(125, 125),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(0),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 125,
                      width: 125,
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 8.0),
                        child: ElevatedButton(
                          onPressed: () {},
                          child: Text('Installation'),
                          style: ElevatedButton.styleFrom(
                            fixedSize: Size(125, 125),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(0),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 8,
              ),
              SocialSignIn(
                  buttonColor: Colors.green,
                  textColor: Colors.white,
                  imageAsset: 'images/plus-16.png',
                  onPressed: () {},
                  buttonName: 'New Enquires'),
            ],
          ),
        ),
      ],
    );
  }

  Widget _search() {
    return Container();
  }
}
