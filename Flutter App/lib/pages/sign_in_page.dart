// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, use_key_in_widget_constructors, unused_element

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:tekroi_project1/alerts/show_exception_alert_dialog.dart';
import 'package:tekroi_project1/buttons/sign_in_button.dart';
import 'package:tekroi_project1/buttons/social_sign_in_button.dart';
import 'package:tekroi_project1/pages/sign_in_bloc.dart';
import 'package:tekroi_project1/pages/Email%20Sign%20In/sign_in_with_email.dart';

class SignInPage extends StatelessWidget {
  final SignInBloc bloc;
  const SignInPage({required this.bloc});

  void _emailSignIn(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        fullscreenDialog: true,
        builder: (context) => EmailSignIn(),
      ),
    );
  }

  Future<void> _googleSignIn(BuildContext context) async {
    try {
      await bloc.googleSignIn();
    } on FirebaseAuthException catch (e) {
      if (e.code == 'ERROR_USER_CANCELLED') {
        return;
      } else {
        showExceptionAlert(context, title: 'Sign in Cancelled', exception: e);
      }
    }
  }

  Future<void> _signInAnonymously(BuildContext context) async {
    try {
      await bloc.signInAnonymously();
    } on FirebaseAuthException catch (e) {
      showExceptionAlert(context, title: 'error sign in', exception: e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('TEKROI'),
        centerTitle: true,
        backgroundColor: Colors.indigo,
      ),
      body: StreamBuilder<bool>(
        stream: bloc.isLoadingStream,
        initialData: false,
        builder: (context, snapshot) {
          return buildContent(context, snapshot.data);
        },
      ),
    );
  }

  Widget buildContent(BuildContext context, bool? isLoading) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _buildHeader(isLoading!),
          SizedBox(height: 48),
          SocialSignIn(
            imageAsset: 'images/google-logo.png',
            onPressed: () => isLoading ? null : _googleSignIn(context),
            buttonName: 'Sign in with Google',
          ),
          SizedBox(height: 8),
          SignInButton(
            onPressed: () => isLoading ? null : _emailSignIn(context),
            buttonName: 'Sign in with Email',
            buttonColor: Colors.teal,
            textColor: Colors.white,
          ),
          SizedBox(height: 8),
          SignInButton(
            onPressed: () => isLoading ? null : _signInAnonymously(context),
            buttonName: 'Sign in Anonymously',
            buttonColor: Colors.blue,
            textColor: Colors.white,
          ),
        ],
      ),
    );
  }

  Widget _buildHeader(bool isLoading) {
    if (isLoading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    return Text(
      'Sign in',
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 32,
        fontWeight: FontWeight.w400,
      ),
    );
  }
}
