// ignore_for_file: empty_catches, prefer_typing_uninitialized_variables, dead_code, unused_element

import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:tekroi_project1/services/auth.dart';

class SignInBloc {
  SignInBloc({required this.auth});
  final AuthBase auth;

  final StreamController<bool> _isLoadingController = StreamController<bool>();

  Stream<bool> get isLoadingStream => _isLoadingController.stream;

  void dispose() {
    _isLoadingController.close();
  }

  void _setIsLoading(bool isLoading) => _isLoadingController.add(isLoading);

  Future<User?> _signIn(Future<User?> Function() signInMethod) async {
    try {
      _setIsLoading(true);
      return await signInMethod();
    } catch (e) {
      _setIsLoading(false);
      rethrow;
    }
  }

  Future<User?> googleSignIn() async => await _signIn(auth.googleSignIn);
  Future<User?> signInAnonymously() async =>
      await _signIn(auth.signInAnonymously);
}
