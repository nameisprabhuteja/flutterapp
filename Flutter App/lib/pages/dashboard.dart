// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, unnecessary_new

import 'package:flutter/material.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: Drawer(
          child: ListView(
            children: [
              Text('Home'),
            ],
          ),
        ),
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: AppBar(
            actions: [
              SizedBox(
                height: 30,
                width: 30,
                child: Image.asset(
                  'images/notification.png',
                ),
              ),
            ],
            backgroundColor: Colors.blue,
            title: Row(
              children: [
                SizedBox(
                  height: 50,
                  width: 50,
                  child: new OverflowBox(
                      minWidth: 0.0,
                      minHeight: 0.0,
                      maxWidth: double.infinity,
                      child: new Image(
                          image: new AssetImage('images/profile-logo.png'),
                          fit: BoxFit.cover)),
                ),
                Text('Anonymous'),
              ],
            ),
          ),
        ));
  }
}
