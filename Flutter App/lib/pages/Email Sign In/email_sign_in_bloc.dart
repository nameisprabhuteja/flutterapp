import 'dart:async';

import 'package:tekroi_project1/pages/Email%20Sign%20In/email_sign_in_model.dart';
import 'package:tekroi_project1/services/auth.dart';

class EmailSignInBloc {
  EmailSignInBloc({required this.auth});
  final AuthBase auth;
  final StreamController<EmailSignInModel> _modelController =
      StreamController();

  Stream<EmailSignInModel> get modelStream => _modelController.stream;
  EmailSignInModel _model = EmailSignInModel();

  void dispose() {
    _modelController.close();
  }

  Future<void> submit() async {
    try {
      updateWith(isLoading: true, submitted: true);
      if (_model.formType == EmailSignInFormType.signIn) {
        await auth.signInWithEmailPassword(_model.email, _model.password);
      } else {
        await auth.createWithEmailPassword(_model.email, _model.password);
      }
    } catch (e) {
      updateWith(isLoading: false);
      rethrow;
    }
  }

  void updateEmail(String email) => updateWith(email: email);

  void updatePassword(String password) => updateWith(password: password);

  void toggleFormTypes() {
    final formType = _model.formType == EmailSignInFormType.register
        ? EmailSignInFormType.signIn
        : EmailSignInFormType.register;
    updateWith(
      email: '',
      password: '',
      formType: formType,
      submitted: false,
      isLoading: false,
    );
  }

  void updateWith({
    String? email,
    String? password,
    EmailSignInFormType? formType,
    bool? isLoading,
    bool? submitted,
  }) {
    _model = _model.copyWith(
      email: email,
      password: password,
      formType: formType,
      isLoading: isLoading,
      submitted: submitted,
    );
    _modelController.add(_model);
  }
}
